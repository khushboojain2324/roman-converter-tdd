package com.thoughtworks.vapasi;

import java.util.HashMap;
import java.util.Map;

public class RomanConverter {

    static HashMap<Character, Integer> romanNumberMap = new HashMap() {
        {
            put('I', 1);
            put('V', 5);
            put('X', 10);
            put('L', 50);
            put('C', 100);
            put('D', 500);
            put('M', 1000);
        }
    };

    public Integer convertRomanToArabicNumber(String roman) {

        int result = 0;
        int current ;
        int previous = 0;

        int i = roman.length() - 1;
        while (i >= 0) {
            try {
                current = romanNumberMap.get(roman.charAt(i));
                result = current >= previous ? result + current : result - current;
            } catch (NullPointerException e) {
                throw new IllegalArgumentException("Invalid roman character");
            }
            previous = current;
            i--;
        }

        return result;
    }
}

